//Execute node FILE.js to execute file
var fs = require('fs');
var path = require('path');
var http = require('http');

var rimraf = require('rimraf');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const imageminGifsicle = require('imagemin-gifsicle');
const getSize = require('get-folder-size');

const sharp = require('sharp');
var sizeOf = require('image-size');

const cheerio = require('cheerio');

var rootPath = "../src/assets/html"
//var rootPath = "assets - Copy"

// remove reference folder
//rmRefDirectoriesRecursive(rootPath);
//compressImgRecursive(rootPath)

var deleteDirs = ["React/Framework/Sample", "React/Framework/fmk_update", "React/Project Sharing/image"]
var deleteExt = ["pdf", "pptx", "exe", "mp4", "zip", "xlsx"]

LoopDirectoriesRecursive(rootPath)

function LoopDirectoriesRecursive(inputPath){
	fs.readdir( inputPath, function( err, files ) {
		
		compressImg(inputPath)
		
        for(var file of files){               
			var subInputPath = inputPath+"/"+file
			getFileSize(subInputPath) 
            if(fs.lstatSync(subInputPath).isDirectory()){	
				if(file === "ref" || file==="bu" || isDeleteDir(subInputPath))
					rmDirectory(subInputPath)
				else
					LoopDirectoriesRecursive(subInputPath);					
            } 
			// else if(getFiletype(file) === "html")
			// 	addTargetToAHrefInHtmlFile(subInputPath)
			else if(deleteExt.indexOf(getFiletype(file)) > -1){
				fs.unlinkSync(inputPath+"/"+file)
				console.log(inputPath+"/"+file + " deleted!")
			}
        }
    });
}


function rmDirectory(path){
	rimraf(path, () => { console.log('delete: ' + path); }); //delete ref
}

function compressImg(path){
	imagemin([path + '/*.{jpg,jpeg,png,gif}'], path, {
	//   imagemin([path + '/*.jpg'], path, {		
		plugins: [
			imageminMozjpeg({quality: 10 }),
			// imageminPngquant({quality: '10-20'}),
			imageminPngquant({quality: 1}),
			imageminGifsicle({optimizationLevel: '3', colors: 4})
		]
	  }).then(files => {
		if(files.length>0)
			files.forEach(file=>{console.log(file.path)});
		//=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …]
	  }).catch(err => {
		console.log(err)
	  });	
}

function addTargetToAHrefInHtmlFile(path){
	fs.readFile(path, 'utf8', function(err, data) {
		if(data){
			data = data.replace(/<a href=\"https/gi, "<a target='_top' href=\"https")
			fs.writeFile(path, data, ()=> {console.log("Update ahref success " + path)} )	
		}
	});
}


/* Common function */

function getFiletype(path){
	var idx = path.lastIndexOf(".") +1
	return path.slice(idx, path.length).toLowerCase()
}

function isDeleteDir(subInputPath){	
  for (var dir of deleteDirs){
	  if(rootPath+"/"+dir === subInputPath)
		return true
  }
  return false
}

function getFileSize(dir){
	getSize(dir, (err, size) =>{
		var mbSize = (size / 1024 / 1024).toFixed(2)
		if(mbSize > 0.5)
		  console.log(mbSize + ' MB', dir)
	})
}
	
/* 
===============================================================
function currently not in use
===============================================================
*/
// find all file and upload to mongodb
function rmRefDirectoriesRecursive(inputPath){    
	fs.readdir( inputPath, function( err, files ) {
	   if(err) {
		   console.error( "Could not list the directory.", err );
		   process.exit(1);
	   }
	   for(file of files){               
		   if(fs.lstatSync(inputPath+"/"+file).isDirectory()){
			   var dirPath = inputPath+"/"+file 
			   if(file === "ref")
				   rimraf(dirPath, function () { console.log('delete: ' + dirPath); });
			   else
				   rmRefDirectoriesRecursive(dirPath);					
		   }            
	   }
   });
}

function compressImgRecursive(inputPath){
    fs.readdir( inputPath, function( err, files ) {
        if(err) {
            console.error( "Could not list the directory.", err );
            process.exit(1);
        }		
				
		compressImg(inputPath)
        for(file of files){
			var fullFilePath = inputPath+"/"+file
			if(file === "ref")
				continue;
            if(fs.lstatSync(fullFilePath).isDirectory())
				compressImgRecursive(fullFilePath);				
			else
				compressImg_sharp(fullFilePath)
        }
    });
}

// compressImg & compressImg_sharp are similar, but compressImg has smaller output size
function compressImg_sharp(path){
	if( ["png", "jpg", "jpeg"].includes(getFiletype(path)) ){	
		var dimensions = sizeOf(path);
		if(dimensions.width > 300){
			sharp(path)
			  .resize(300)
			  .toBuffer(function(err, buffer){
				fs.writeFile(path, buffer, (e) => { console.log("compress done! " + path)})  
			})			
		}

	}
}
