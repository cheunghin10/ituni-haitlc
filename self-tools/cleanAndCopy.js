//Execute node FILE.js to execute file
var fs = require('fs');
var path = require('path');
var http = require('http');

var rimraf = require('rimraf');
var ncp = require('ncp').ncp;

var zipPath = "../haitlc.zip"
//var networkPath = "\\\\dc6eshdns01b\\SA1_DEP\\EA1\\haitlc\\static"
var networkPath = "\\\\wcdcharch03\\haitlc\\static" 
var rootPath = "../src/assets/html"
//var rootPath = "assets - Copy"

// remove reference folder
//rmRefDirectoriesRecursive(rootPath);
//compressImgRecursive(rootPath)

//-------------------------real code
rmHaitlcZipPromise(zipPath)
	.then( () => {
		return rmRefDirectoriesPromise(rootPath)
	})
	.then( () => {
		return copySourceFromNetworkPromise(networkPath)
	})
	.catch( (err) => {
		printErrAndExit(err)
	})

function rmHaitlcZipPromise(path){
	
	return new Promise( (resolve, reject) => {
		if(fs.existsSync(path)){
			fs.unlink(path, (err) => {
				if(err)
					reject(err)
				console.log('file deleted: '+path)
				resolve()
			})
		} else {
			console.log('file not exists: '+path)
			resolve()
		}		
	})
}

function rmRefDirectoriesPromise(path){
	return new Promise( (resolve, reject) => {
		rimraf(path, (err) => {
			if(err)
				reject(err)		
			console.log('directory deleted: ' + path);
			resolve()
		});
	})
}

function copySourceFromNetworkPromise(path){
	return new Promise( (resolve, reject) => {
		console.log("Start copy...")
		ncp(networkPath, rootPath, function (err) {
			if(err)
				reject(err)		
			console.log("Copy done!")
			resolve()
		})	
	})
}


/* Common function */

function printErrAndExit(err){
	console.error(err)
	process.exit(1);
}

function getFiletype(path){
	var idx = path.lastIndexOf(".") +1
	return path.slice(idx, path.length).toLowerCase()
}


	

	
	

