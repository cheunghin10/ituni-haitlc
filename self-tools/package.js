// require modules
var fs = require('fs');
var archiver = require('archiver');
var ncp = require('ncp').ncp;
var rimraf = require('rimraf');
const md5File = require('md5-file')

var desPath = "assets"

ncp("../platforms/android/assets", desPath, function (err) {
	if (err) { return console.error(err);}
	console.log("copy done!")
	
	new Promise((resolve, reject) => { 
		deleteCordovaAndPlugins(resolve)
	}).then(() => { // need to return new promise  
		new Promise((resolve) => {
			zipFile(resolve)
		}).then(() => {
			hashFile()
		})
	})
});


function deleteCordovaAndPlugins(resolve){
	rimraf(desPath+"/www/cordova-js-src", function () { 
		console.log('delete: ' + desPath+"/www/cordova-js-src"); 
		rimraf(desPath+"/www/plugins", function () { 
			console.log('delete: ' + desPath + "/www/plugins"); 
			fs.unlinkSync(desPath+'/www/cordova.js');
			fs.unlinkSync(desPath+'/www/cordova_plugins.js');
			resolve()
		});		
	});
}


function zipFile(resolve){
	// create a file to stream archive data to.
	var output = fs.createWriteStream('haitlc.zip');
	var archive = archiver('zip', {
	  zlib: { level: 9 } // Sets the compression level.
	});
	 
	// listen for all archive data to be written
	// 'close' event is fired only when a file descriptor is involved
	output.on('close', function() {
	  console.log(archive.pointer() + ' total bytes');
	  console.log('archiver has been finalized and the output file descriptor has closed.');
	  resolve()
	});
	 
	// good practice to catch warnings (ie stat failures and other non-blocking errors)
	archive.on('warning', function(err) {
	  if (err.code === 'ENOENT') {
		// log warning
	  } else {
		// throw error
		throw err;
	  }
	});

	// good practice to catch this error explicitly
	archive.on('error', function(err) {
	  throw err;
	});

	// pipe archive data to the file
	archive.pipe(output);
	archive.directory(desPath, "assets");
	archive.finalize();
}


function hashFile(){
	/* Async usage */
	md5File('haitlc.zip', (err, hash) => {
	  if (err) throw err
	  console.log('\x1b[36m%s\x1b[0m', "The MD5 sum of haitlc.zip is: "+ hash)
	})
}