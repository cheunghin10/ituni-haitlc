//Execute node FILE.js to execute file
var fs = require('fs');
var path = require('path');
var http = require('http');

var rimraf = require('rimraf');
const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminGiflossy = require('imagemin-giflossy');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminMozjpeg = require('imagemin-mozjpeg');
const getSize = require('get-folder-size');

const sharp = require('sharp');
var sizeOf = require('image-size');

const cheerio = require('cheerio');

var rootPath = "C:/angular-test/ituni-haitlc/src/assets/html"
//var rootPath = "assets - Copy"

// remove reference folder
//rmRefDirectoriesRecursive(rootPath);
//compressImgRecursive(rootPath)

var deleteDirs = ["React/Framework/Sample", "React/Framework/fmk_update"]
var deleteExt = ["pdf", "pptx", "exe", "mp4"]

compressImg("./assets")

function rmDirectory(path){
	rimraf(path, () => { console.log('delete: ' + path); }); //delete ref
}

function compressImg(path){
	//imagemin([path + '/*.{jpg,png,gif}'], path, {
        console.log("comporessing")
	  imagemin([path + '/*.jpg'], path, {		
		plugins: [
			imageminMozjpeg({quality: 20 }),
		// 	imageminPngquant({quality: '10-30'}),
		// 	imageminGifsicle({optimizationLevel: "1", colors: 4})
		]
	  }).then(files => {
          console.log(files)
		//=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …]
	  }).catch(err => {
		console.log(err)
	  });	
}

function addTargetToAHrefInHtmlFile(path){
	fs.readFile(path, 'utf8', function(err, data) {
		if(data){
			data = data.replace(/<a href=\"https/gi, "<a target='_top' href=\"https")
			fs.writeFile(path, data, ()=> {console.log("Update ahref success " + path)} )	
		}
	});
}


/* Common function */

function getFiletype(path){
	var idx = path.lastIndexOf(".") +1
	return path.slice(idx, path.length).toLowerCase()
}

function isDeleteDir(subInputPath){	
  for (var dir of deleteDirs){
	  if(rootPath+"/"+dir === subInputPath)
		return true
  }
  return false
}

function getFileSize(dir){
	getSize(dir, (err, size) =>{
		var mbSize = (size / 1024 / 1024).toFixed(2)
		if(mbSize > 0.5)
		  console.log(mbSize + ' MB', dir)
	})
}
	