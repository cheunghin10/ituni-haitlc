import { Component } from '@angular/core';
import { Events, AlertController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { Config } from '../../shared/service';
import { FetchImagePipe } from '../../shared/fetch-image.pipe';
import { HttpClientService } from '../../shared/services/http-client.service';

declare var webview: any;

var me

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  imgPath: string;
  me: Object

  HAITLC_DIR = "haitlc"

  constructor(public events: Events, 
              private alertCtrl: AlertController,
              public config: Config, 
              private iab: InAppBrowser,
              private file: File, 
              private fileOpener: FileOpener,
              public fetchImagePipe: FetchImagePipe,
              private httpClientService: HttpClientService
            ) {
    me = this
    window.addEventListener("message", this.onMessage, false);    
  }

  onMessage(event) {    
    console.log("in listener", event.data.path)
    if(event.data.path){
      me.saveAndOpenFile(event.data.path)//me.openInIAB(event.data.path) 
    }
  }

  getStoredFileBasePath(){
    return this.file.dataDirectory + this.HAITLC_DIR + "/"
  }
  

  addGWToPath(fullPath){
    var sliceLocation = fullPath.indexOf("/file/") +1
    var netPath = `${this.config.getIGWUrl()}/haitlc/Big Data/${fullPath.slice(sliceLocation, fullPath.length)}`
    console.log("NetPath: ", netPath)
    return netPath
  }

  saveAndOpenFile(fullPath){
    // temp disable script as ITUNI does not install ionic plugin to open PDF/ppt by other app
    let alert = this.alertCtrl.create({
      title: 'File not found!',
      subTitle: 'The file is not found. Please try again later',
      buttons: ['Dismiss']
    });
    alert.present();    
  }


  saveFileToDevice(fileName, blob){
    this.file.checkDir(this.file.dataDirectory, this.HAITLC_DIR)
    .then( () => { return this.saveFile(fileName, blob) })
    .then( () => { return this.openFile(fileName, blob.type) })
    .catch((err) => {
      this.fileOpenErrorHandle(err)
      return this.createDirAndSaveFile()             
        .then( () => { return this.saveFile(fileName, blob)})
        .then( () => { return this.openFile(fileName, blob.type)})
        .catch( err => { this.fileOpenErrorHandle(err) })      
    })        
  }

  fileOpenErrorHandle(err){
    console.log(err)
    if(err.messsage == "Activity not found: No Activity found to handle Intent { act=android.intent.action.VIEW dat=content://hk.org.ha.haitlc.opener.provider/files/haitlc/samplepptx.pptx typ=application/vnd.openxmlformats-officedocument.presentationml.presentation flg=0x40000000 }"){
      let alert = this.alertCtrl.create({
        title: 'Cannot open file',
        subTitle: 'You may not have application in your device to open the file.',
        buttons: ['Dismiss']
      });
      alert.present();        
    }
  }

  createDirAndSaveFile(){
    return this.file.createDir(this.file.dataDirectory, this.HAITLC_DIR, true)    
  }

  saveFile(fileName, blob){
    return this.file.writeFile(this.getStoredFileBasePath(), fileName, blob, { replace: true })
  }

  openFile(fileName, blobType){
    console.log(`${this.getStoredFileBasePath()}${fileName}`, blobType)
    return this.fileOpener.open(`${this.getStoredFileBasePath()}${fileName}`, blobType);
  }

  /* ------------------------------------------ Testing block ------------------------------------------ */
  openInIAB(path) {
    this.getDocPath(path)
        .then( (docPath) => {
          console.log("docPath: ", docPath)       
          this.iab.create(docPath, '_system');
        })
        .catch( (e) => {
          console.log(e)
        })
  }

  getDocPath(path){
    //return this.fetchImagePipe.transform(`${this.config.getIGWUrl()}/pp/images/CMS/cms-main.png`);
    var sliceLocation = path.indexOf("/file/") +1
    var netPath = path.slice(sliceLocation, path.length) 
    console.log("NethPath: ", netPath)
    // return this.fetchImagePipe.transformPromise(`${this.config.getIGWUrl()}/haitlc/Big Data/${netPath}`);
    return this.fetchImagePipe.transformPromise(`${this.config.getIGWUrl()}/haitlc/Big Data/file/2017-08-04_Spark/2017-08-04_Big_Data_Enablement_Training_Spark_v1r6.pdf`);
  } 

  test(path) {    
      this.iab.create(path, '_system');
  }

  /* ------------------------------------------ END Testing block ------------------------------------------ */
    
  exitApp() {
    webview.close();
  }
}
