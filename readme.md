# HAITLC Mobile app
Moblie version of HAITLC.home. The source code is divided into 2 parts:

1. Ionic app 

    The ionic app is container for HAITLC html files. Content of HAITLC is in `src/assets/html`

2. self-tools 

    This sub-project is a helper tool to copy content from HAITLC server, process images, delete large files, build and package as zip.

## Setup 

1. At root run: `npm i`
2. `cd self-tools`
3. `npm i`


## Get content from HAITLC and Build

**May need npm 7 to run**

1. `npm run ituni-cleancopy`, Clean local & copy from haitlc
2. `npm run ituni-process`, Delete large files, compress image
3. `npm run ituni-build` (**Need npm 8 to run**), build source code
4. `npm run ituni-package`, remove unwanted file, generate MD5 hash

## Develop
1. `npm run run-local:window`

## Upload to ITUNI - DEV
1. Go to http://eao-keystonejs-sit:3333/, login ea1@ha.org.hk / abc123$$
2. `App` at top menu > `HAITLC`
3. Update the highlighted field and save
![](readme/haitlc-update-1.PNG)
4. Check in ITUNI DEV app


## Backup of self-developed code
Moved to `/self-tools/backup/home.ts`

## Troubleshoot

Cordova proxy error:
- Use local gradle `set CORDOVA_ANDROID_GRADLE_DISTRIBUTION_URL C:\gradle\gradle-3.3-all.zip`