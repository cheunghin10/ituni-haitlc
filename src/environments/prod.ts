export const prodConfig = {
    isProduction: true,
    environment: 'prod',
    keystoneUrl: 'https://eaoigw-corp-eamobile-prd.app.ha.org.hk:34408/eao-ituni-igw/1_0_0/service/cms',
    igwUrl: 'https://eaoigw-corp-eamobile-prd.app.ha.org.hk:34408/eao-ituni-igw/1_0_0/service',
    appVersion: '2.0.0',
    testUrl: 'http://localhost:7001/eao-pp-core/1_0_0'
};
