import { NgModule } from '@angular/core';
import { isDevMode } from '@angular/core';
import { EnvVariables  } from './environments.token';
import { localConfig } from './local';
import { devConfig } from './dev';
import { prodConfig } from './prod';

declare const process: any; // Typescript compiler will complain without this

export function environmentFactory() {
    console.log(process, isDevMode())
    if(process.env.HAITLC_ENV){
        const haitlcEnv = process.env.HAITLC_ENV.trim();
        console.log(`HAITLC_ENV: ${haitlcEnv}`);
        switch(haitlcEnv) {
            case 'prod':
                return prodConfig;
            case 'dev':
                return devConfig;
            default:
                return localConfig;
        }
    } else{
        console.log(`HAITLC_ENV: No config found, use dev config`, devConfig);
        return devConfig
    }
}

@NgModule({
  providers: [
    {
      provide: EnvVariables,
      // useFactory instead of useValue so we can easily add more logic as needed.
      useFactory: environmentFactory
    }
  ]
})
export class EnvironmentsModule {}
