export const localConfig = {
    isProduction: false,
    environment: 'local',
    keystoneUrl: 'https://eaoigw-corp-eamobile-sit.appdev.ha.org.hk:16408/eao-ituni-igw/1_1_0/service/route/cms',
    igwUrl: 'https://eaoigw-corp-eamobile-sit.appdev.ha.org.hk:16408/eao-ituni-igw/1_1_0/service',
    appVersion: '2.0.0',
    keystoneAuthHeader: 'Basic ZGV2QGhhLm9yZy5oazphYmMxMjMkJA==',
    testUrl: 'http://localhost:7001/eao-pp-core/1_0_0'
};
