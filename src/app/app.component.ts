import { Component, Inject } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { EnvVariables } from '../environments/environments.token';
import { CommonService } from '../shared/services/common.service';
import { Config } from '../shared/service';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private commonService: CommonService,
    @Inject(EnvVariables) public envConfig) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      // Initize env variable
      this.commonService.appConfig = {
        ...Config,
        ...this.envConfig
      };
      console.log(this.commonService.appConfig);      
    });
  }


}

