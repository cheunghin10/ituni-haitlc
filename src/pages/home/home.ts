import { Component } from '@angular/core';

declare var webview: any;

var me

/*
****************************************************
source code moved to /self-tools/backup/home.ts
****************************************************
*/ 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  imgPath: string;
  me: Object

  HAITLC_DIR = "haitlc"

  constructor() {
    me = this 
  }

  
    
  exitApp() {
    webview.Close();
  }
}
