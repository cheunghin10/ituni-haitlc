import { Pipe, PipeTransform } from '@angular/core';

import { HttpClientService } from './services/http-client.service';
import { CommonService } from './services/common.service';

@Pipe({
  name: 'fetchImage',
  pure: false
}) 
export class FetchImagePipe implements PipeTransform {        
    cachedData;
    cachedUrl;


    constructor(private httpClientService: HttpClientService, private commonService: CommonService) {} 

    transform(url: string): any {
        let data = this.commonService.imageCache[url];
        if (data) {
            this.cachedData = data;         
        }
        else {    
            this.cachedData = 'assets/imgs/loading.gif';
            if(url !== this.cachedUrl) {                
                this.httpClientService.getDocument(this.toInternetGatewayUrl(url))            
                .subscribe(
                    result => {
                        this.cachedData = result
                        this.commonService.imageCache[url] = result;
                    },
                    e => {
                        this.cachedData = url
                        this.commonService.imageCache[url] = url;
                    }
                );
            }
            this.cachedUrl = url;
        } 
        return this.cachedData;
    }


    transformPromise(url: string): any {
        let data = this.commonService.imageCache[url];
        if (data) {
            this.cachedData = data
            return Promise.resolve(this.cachedData);         
        }
        else {    
            this.cachedData = 'assets/imgs/loading.gif';
            return new Promise( (resolve, reject) => {
                if(url !== this.cachedUrl) {                
                this.httpClientService.getDocument(this.toInternetGatewayUrl(url))            
                    .subscribe(
                        result => {
                            this.cachedData = result
                            this.commonService.imageCache[url] = result;
                            resolve(result);
                        },
                        e => {
                            this.cachedData = url
                            this.commonService.imageCache[url] = url;
                            reject(e);
                        }
                    );
                }
                this.cachedUrl = url;
                return this.cachedData;
            })
        } 
    }

    toInternetGatewayUrl(url) {        
        return url;
    }

}
