import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

import { CommonService } from './services/common.service'

declare var webview: any;

@Injectable()
export class Config {
  imgBasePath             = "assets/images/plist/";               //full path: "assets/images/plist/clinical/CID.png"
  productImgBasePath      = "assets/images/product-img/";         //full path: "assets/images/plist/clinical/CID.png"
  testProductImgBasePath  = "assets/images/product-img/test/";    //full path: "assets/images/plist/clinical/CID.png"
  secureStorageName       = 'secureStorage';
  trackingId              = 'UA-102486264-1';
  products;


  constructor(private commonService: CommonService, public plt: Platform) {
  }

  getIGWUrl(){
    if(this.isBuildEnv() &&  webview!== null)
      return (webview.getItuniData().igwUrl + '/route/cms')
    else 
      return this.commonService.appConfig.keystoneUrl
  }

  isBuildEnv(){
    return this.plt.is("cordova")
  }

}