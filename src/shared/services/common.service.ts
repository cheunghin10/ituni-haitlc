import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { SecureStorage } from '@ionic-native/secure-storage';
import { Storage } from '@ionic/storage';
import { Platform, LoadingController, AlertController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import Rx from 'rxjs/Rx';

@Injectable()
export class CommonService {
    storagePrefix = 'PP_';

    appConfig;
    userName = '';
    otpChecked = false;
    userPreference = { data: { authentication: 'passcode' } };
    passcode = '';
    password = '';
    isFingerprintAvailable: boolean = false;
    appStates = {};
    apps = [];
    appCategories = [];
    reasonsByCode;
    reasonsByStatus;
    imageCache = {};
    userProfile = {
      user: {
        displayName: '',
        title: ''
      },
      email: ''
    };

    loading;

	constructor(
        private nativeStorage: NativeStorage, private secureStorage: SecureStorage,
        private ionicStorage: Storage, public platform: Platform, private network: Network,
        private loadingController: LoadingController, private alertCtrl: AlertController
    ) {
        this.reasonsByCode = {
            '4530-1000-0-E': 'Invalid OTP',
            '4530-1000-1-E': 'Invalid OTP',
            '4530-1000-2-E000': 'Logon attempt reached limit.',
            '4530-1000-2-E': 'OTP checking error',
            '4530-1001-2-E': 'Device registration failed.',
            '4530-1001-3-E': 'Authentication failed.',
            '4530-1001-4-E': 'User info retrieval failed.'
        }

        this.reasonsByStatus = {
            0: 'Unable to connect to server',
            404: 'Service is not available',
            500: 'Service error'
        }
    }

    initialize() {
        let getUserName = this.getStorageItem('userName')
            .then(data => this.userName = data)
            .catch(e => this.userName = '');

        let getUserPreference = this.getStorageItem('userPreference')
            .then(data => data? this.userPreference = data : null)
            .catch(e => null);

        let getOtpChecked = this.getStorageItem('otpChecked')
            .then(data => this.otpChecked = data)
            .catch(e => this.otpChecked = false);

        let getUserProfile = this.getStorageItem('userProfile')          
            .then(data => {
                if (data && data.user) {
                this.userProfile = data;
                }
            })
            .catch(e => null);

        let checkFingerprintAvailability = this.checkFingerprintAvailability()
            .then(res => this.isFingerprintAvailable = res)
            .catch(e => this.isFingerprintAvailable = false);

        let getAppStates = this.getStorageItem('appStates')
            .then(appStates => this.appStates = {...appStates })
            .catch(e => this.appStates = {});

        let getApps = this.getStorageItem('apps')
            .then(apps => this.apps = [...apps])
            .catch(e => this.apps = []);

        let getAppCategories = this.getStorageItem('appCategories')
          .then(appCategories => this.appCategories = [...appCategories])
          .catch(e => this.appCategories = []);

        let getSecureInfo;
        if (this.platform.is('cordova')){
          getSecureInfo = this.secureStorage.create(`${this.storagePrefix}${this.appConfig.secureStorageName}`)
            .then(ss => {
              return Promise.all([
                ss.get('passcode')
                  .then(passcode => this.passcode = passcode)
                  .catch(e => this.passcode = ''),

                ss.get('password')
                  .then(password => this.password = password)
                  .catch(e => this.password = '')
              ]);
            });
        }
        else {
          getSecureInfo = Promise.all(
            [this.getStorageItem('passcode')
              .then(passcode => this.passcode = passcode)
              .catch(e => this.passcode = ''),
            this.getStorageItem('password')
              .then(password => this.password = password)
              .catch(e => this.password = '')
            ]);
        }

        return Promise.all([
            getUserName, getUserPreference, getUserProfile, getOtpChecked,
            checkFingerprintAvailability, getAppStates, getApps,
            getAppCategories, getSecureInfo
        ]);
    }

    commitOtpChecked(checked) {
        this.otpChecked = checked;
        return this.setStorageItem('otpChecked', this.otpChecked);
    }

    commitUserName(userName) {
        this.userName = userName;
        return this.setStorageItem('userName', this.userName);
    }

    commitPassword(password) {
        this.password = password;
        return this.setSecureStorageItem('password',  this.password );
        // return this.setSecureStorageItem.create(`${this.storagePrefix}${this.appConfig.secureStorageName}`)
        // .then(storage => storage.set('password', this.password));
    }

    commitApps(apps) {
        this.apps = [...apps]
        return this.setStorageItem('apps', this.apps);
    }

    commitAppStates() {
        return this.setStorageItem('appStates', this.appStates);
    }

    commitAppCategories(appCategories) {
	    this.appCategories = [...appCategories];
	    return this.setStorageItem('appCategories', this.appCategories);
    }

    commitUserPreference() {
        return this.setStorageItem('userPreference', this.userPreference);
    }

    commitUserProfile(userProfile){
	      this.userProfile = userProfile;
	      return this.setSecureStorageItem('userProfile', this.userProfile);
    }

    commitPasscode(passcode) {
        this.passcode = passcode;
        return this.setSecureStorageItem('passcode', this.passcode);
        // return this.setSecureStorageItem.create(`${this.storagePrefix}${this.appConfig.secureStorageName}`)
        // .then(storage => storage.set('passcode', this.passcode));
    }

    getAppState(app) {
        this.appStates[app._id] = {
            downloaded: false,
            isBookmark: false,
            iconPath: '',
            ...this.appStates[app._id]
        }

        return this.appStates[app._id];
    }

    checkFingerprintAvailability() {
        return Promise.resolve(false);
    }

    showLoading(msg) {
        this.loading = this.loadingController.create({
        content: msg
        });
        this.loading.present();
    }

    dismissLoading() {
        this.loading.dismiss();
    }

    catchError(e) {
        console.log(e);
        let response = {
            responseCode: '',
            reason: 'Unknown error'
        }
        
        try {
            const status = e.status;
            response = {
                ...response,                
                ...e.json()
            }
            
            let reason = this.reasonsByCode[response.responseCode] ||
                    this.reasonsByCode[response.responseCode.slice(0, -3)] ||
                    this.reasonsByStatus[status] ||
                    'Unknown error';
            
            response.reason = reason;
            
        }
        catch(e) {}
        
        return Rx.Observable.throw(response);
    }


    alert(msgOptions = {}) {
        let alert = this.alertCtrl.create({
            title: '',
            subTitle: '',
            buttons: ['OK'],
            enableBackdropDismiss: false,
            ...msgOptions
        });

        alert.present();
    }

    private setSecureStorageItem(key: string, value: any){
        if (this.platform.is('cordova')){
            return this.secureStorage.create(`${this.storagePrefix}${this.appConfig.secureStorageName}`)
            .then(storage => storage.set(key, value));
        }
        else {
            return this.setStorageItem(key, value);
        }
    }

    private getStorageItem(key: string){
      if (this.platform.is('cordova')) {
        return this.nativeStorage.getItem(`${this.storagePrefix}${key}`);
      }
      else {
        return this.ionicStorage.get(`${this.storagePrefix}${key}`);
      }
    }

    private setStorageItem(key: string, value: any){
      if (this.platform.is('cordova')) {
        return this.nativeStorage.setItem(`${this.storagePrefix}${key}`, value);
      }
      else {
        return this.ionicStorage.set(`${this.storagePrefix}${key}`, value);
      }
    }

    getNetworkType() {
        return this.network.type;
    }

    isLocalEnviroment() {
        return this.appConfig.environment === 'local';
    }
}
